from models.source_table import source_table
from models.session import Session
from sqlalchemy import create_engine
import argparse


def main(connection_url, source_table_name):
    """update deprecated_sources in specified protocol databases"""
    DataSourceIngest = source_table(source_table_name)

    # start database engine
    engine = create_engine(connection_url, echo=True)
    Session.configure(bind=engine)
    session = Session()

    # get data from source and update last modification datetime
    for source in session.query(DataSourceIngest):
        source.get_data_from_source()

    # commit session
    session.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-conn', '--connection_url', type=str, default='sqlite:///:memory:',
                        help='database url')
    parser.add_argument('-table', '--source_table', type=str, default='covid19_sources', help='source table name')
    args = parser.parse_args()

    main(args.connection_url, args.source_table)

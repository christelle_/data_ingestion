from minio import Minio
from minio.error import ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists
import os

# credentials
access_key = os.environ['MINIO_ACCESS_KEY']
secret_key = os.environ['MINIO_SECRET_KEY']
endpoint_url = os.environ['ENDPOINT_URL']
# access_key = "AKIAIOSFODNN7TESTMINIO"
# secret_key = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYTESTMINIO"
# endpoint_url = "127.0.0.1:9000"
mc = Minio(endpoint=endpoint_url, access_key=access_key, secret_key=secret_key,
           session_token=None, region=None, http_client=None, secure=False)


def create_s3_bucket(bucket_name):
    print("creating bucket %s ..." % bucket_name)
    try:
        mc.make_bucket(bucket_name, location="us-east-1")
    except BucketAlreadyOwnedByYou:
        print("Bucket %s is already owned by you." % bucket_name)
    except BucketAlreadyExists:
        print("Bucket %s already exists." % bucket_name)
    except ResponseError:
        raise


def put_s3_object(source, buffer, size):
    print(f"putting object {source['storage_info']['object_name']} ...")
    try:
        mc.put_object(bucket_name=source['storage_info']['bucket_name'],
                      object_name=source['storage_info']['object_name'],
                      data=buffer,
                      length=size,
                      content_type=source['content_type'])
    except ResponseError as err:
        print(err)


def put_s3_file(data_path, object_name, bucket_name):
    # create bucket
    print("create bucket.")
    try:
        mc.make_bucket(bucket_name, location="us-east-1")
    except BucketAlreadyOwnedByYou as err:
        pass
    except BucketAlreadyExists as err:
        pass
    except ResponseError as err:
        raise

    # put data in bucket
    print("put new object.")
    try:
        mc.fput_object(bucket_name=bucket_name,
                       object_name=object_name,
                       file_path="%s/%s" % (data_path, object_name))
    except ResponseError as err:
        print(err)
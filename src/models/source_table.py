from models.session import Session
import source_processing
from utils.s3_helpers import mc
from minio.error import ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from datetime import datetime
import requests
import io


Base = declarative_base()


def source_table(table_name):
    class DataSourceIngest(Base):
        __tablename__ = table_name

        id = Column(String, primary_key=True)
        name = Column(String)
        url = Column(String, nullable=False)
        lastModified = Column(String)
        protocol = Column(String, nullable=False)
        objectUri = Column(String)
        bucketName = Column(String)  # if protocol == s3
        objectName = Column(String)  # if protocol == s3
        tableName = Column(String)   # if protocol == postgre
        ingestion_interval = Column(Integer)
        content_type = Column(String)
        processing_method = Column(String)

        def __repr__(self):
            return f'source: {self.url}'

        @staticmethod
        def create_source(**kwargs) -> 'DataSourceIngest':
            ds = DataSourceIngest(**kwargs)
            Session().add(ds)
            return ds

        def get_data_from_source(self):
            if self.lastModified:
                lastModified = datetime.strptime(self.lastModified, '%d-%m-%Y %H:%M:%S')
                now = datetime.now()
                timeElapsed = (now - lastModified).total_seconds() / 3600.
                # if ingestion time hasn't been reached
                if timeElapsed < self.ingestion_interval:
                    return

            # else, get data
            resp = requests.get(self.url)

            # process data if applicable
            processing_method = getattr(source_processing, self.processing_method, None)
            if processing_method is not None:
                data = processing_method(resp)
            else:
                data = resp.text

            # create buffer and send data to database
            # TODO: generate presigned urls for PUT actions
            csv_bytes = data.encode('utf-8')
            csv_buffer = io.BytesIO(csv_bytes)
            if self.protocol == 's3':
                self.create_s3_bucket()
                self.put_s3_object(buffer=csv_buffer, size=len(csv_bytes))

            # update lastModified datetime
            self.lastModified = datetime.now().strftime('%d-%m-%Y %H:%M:%S')

        def create_s3_bucket(self):
            print("creating bucket %s ..." % self.bucketName)
            try:
                mc.make_bucket(self.bucketName, location="us-east-1")
            except BucketAlreadyOwnedByYou:
                print("Bucket %s is already owned by you." % self.bucketName)
            except BucketAlreadyExists:
                print("Bucket %s already exists." % self.bucketName)
            except ResponseError:
                raise

        def put_s3_object(self, buffer, size):
            print(f"putting object {self.objectName} ...")
            try:
                mc.put_object(bucket_name=self.bucketName,
                              object_name=self.objectName,
                              data=buffer,
                              length=size,
                              content_type=self.content_type)
            except ResponseError as err:
                print(err)

    return DataSourceIngest

# DataSourceIngest.__table__
# source = DataSourceIngest(url='https://www.data.gouv.fr/en/datasets/r/d3a98a30-893f-47f7-96c5-2f4bcaaa0d71',
#                           protocol='s3')

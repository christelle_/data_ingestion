from sqlalchemy.orm import scoped_session, sessionmaker

# allows having different sections of the application to share the same session
Session = scoped_session(sessionmaker())
import json
import os


class DataSourceJson(object):

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', None)
        self.sources = kwargs.get('deprecated_sources', None)
        self.filepath = f'deprecated_sources/{self.name}_sources.json' if self.name is not None else kwargs.get('filepath', None)

    def create_json_file(self, overwrite=False):
        if not overwrite and os.path.exists(self.filepath):
            raise ValueError("The deprecated_sources file you attempt to create already exists. "
                             "To overwrite it, specify overwrite=True. Otherwise, use the update_json_file() method.")
        if isinstance(self.sources, list):
            self.sources = dict(sources=self.sources)
        with open(self.filepath, 'w') as json_file:
            json.dump(self.sources, json_file)

    def update_json_file(self, new_sources):
        with open(self.filepath, 'r') as json_file:
            sources = json.load(json_file)
        sources['deprecated_sources'].extend(new_sources)
        print(sources)
        with open(self.filepath, 'w+') as json_file:
            json.dump(sources, json_file)


# # covid19 example
# covid19_sources = {
#     "deprecated_sources": [
#         {
#             "name": "synthesis_france",
#             "url": "https://www.data.gouv.fr/en/datasets/r/d3a98a30-893f-47f7-96c5-2f4bcaaa0d71",
#             "lastModified": "",  # dd-mm-YYYY HH:MM:SS
#             "protocol": "s3",
#             "storage_info": dict(uri="", bucket_name="covid19", object_name="france/synthesis.csv"),
#             "ingestion_interval": 24,  # in hours
#             "content_type": "application/csv",
#             "processing_method": "covid19_synthesis"
#         }
#     ]
# }
# ds = DataSourceJson(name="covid19", deprecated_sources=covid19_sources)
# ds.create_json_file(overwrite=True)
# ds.update_json_file(new_sources=covid19_sources['deprecated_sources'])
# https://www.kaggle.com/antgoldbloom/covid19-data-from-john-hopkins-university/download

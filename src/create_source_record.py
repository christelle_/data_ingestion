from models.source_table import source_table, Base
from models.session import Session
from sqlalchemy import create_engine
import argparse
import uuid


def main(record_fields, connection_url, source_table_name):
    """create new source record."""
    DataSourceIngest = source_table(source_table_name)

    # start database engine
    engine = create_engine(connection_url, echo=True)
    Session.configure(bind=engine)
    session = Session()

    # create table if it doesn't exist
    Base.metadata.create_all(engine)

    # check if source doesn't already exist
    same_urls = session.query(DataSourceIngest.url).filter(DataSourceIngest.url == record_fields['url']).all()
    if len(same_urls) != 0:
        raise ValueError("Source url already exists in database.")

    # create source
    # TODO: vérifier que l'id n'existe pas déjà
    record_fields['id'] = str(uuid.uuid4())
    DataSourceIngest().create_source(**record_fields)

    # commit session
    session.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-conn', '--connection_url', type=str, default='sqlite:///:memory:',
                        help='source database url')
    parser.add_argument('-table', '--source_table', type=str, default='covid19_sources', help='source table name')
    parser.add_argument('-url', '--url', type=str, default='', help='datasource url')
    parser.add_argument('-name', '--source_name', type=str, default='', help='datasource name')
    parser.add_argument('-last', '--lastModified', type=str, default='',
                        help='last source modification datetime (format: %d-%m-%Y %H:%M:%S)')
    parser.add_argument('-p', '--protocol', type=str, default='s3',
                        help='data ingestion protocol (choices: s3, postgre)')
    parser.add_argument('-uri', '--objectUri', type=str, default='', help='data uri if applicable')
    parser.add_argument('-b', '--bucketName', type=str, default='', help='data bucket name if protocol is s3')
    parser.add_argument('-o', '--objectName', type=str, default='', help='data object name if protocol is s3')
    parser.add_argument('-t', '--tableName', type=str, default='', help='data table name if protocol is postgre')
    parser.add_argument('-int', '--ingestion_interval', type=int, default=24,
                        help='ingestion minimum time interval in hours')
    parser.add_argument('-typ', '--content_type', type=str, default='', help='source content type')
    parser.add_argument('-proc', '--processing_method', type=str, default='',
                        help='processing method before saving to protocol DB (available in source_processing.py')
    args = parser.parse_args()
    record_fields = dict(id=None, name=args.source_name, url=args.url,
                         lastModified=args.lastModified,
                         protocol=args.protocol,
                         objectUri=args.objectUri,
                         bucketName=args.bucketName, objectName=args.objectName,
                         tableName=args.tableName,
                         ingestion_interval=args.ingestion_interval,
                         content_type=args.content_type,
                         processing_method=args.processing_method)

    main(record_fields, args.connection_url, args.source_table)



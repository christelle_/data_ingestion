from utils.s3_helpers import create_s3_bucket, put_s3_object
from deprecated_source_template import DataSourceJson
import source_processing
import glob
from datetime import datetime
import requests
import json
import pandas as pd
import io

# TODO: set up a database of datasources instead of json files

source_files = glob.glob('deprecated_sources/*.json')

if __name__ == '__main__':
    for file in source_files:
        # get json object
        print(f'Getting deprecated_sources from {file}')
        with open(file, 'r') as f:
            sources = json.load(f)

        # ingest each source based on ingestion interval and protocol
        sources_ = sources["deprecated_sources"]
        updated_sources = []
        for source in sources_:
            if source['lastModified']:
                lastModified = datetime.strptime(source['lastModified'], '%d-%m-%Y %H:%M:%S')
                now = datetime.now()
                timeElapsed = (now - lastModified).total_seconds() / 3600.
                # if ingestion time hasn't been reached
                if timeElapsed < source['ingestion_interval']:
                    updated_sources.append(source)
                    continue

            # else create/update data
            resp = requests.get(source['url'])

            # process data if applicable
            processing_method = getattr(source_processing, source['processing_method'], None)
            if processing_method is not None:
                data = processing_method(resp)
            else:
                data = resp.text

            # create buffer and send data to database
            # TODO: generate presigned urls for PUT actions
            csv_bytes = data.encode('utf-8')
            csv_buffer = io.BytesIO(csv_bytes)
            if source['protocol'] == 's3':
                create_s3_bucket(source['storage_info']['bucket_name'])
                put_s3_object(source, buffer=csv_buffer, size=len(csv_bytes))

            # update lastModified datetime
            source['lastModified'] = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            updated_sources.append(source)

        # update json file
        ds = DataSourceJson(filepath=file, sources=updated_sources)
        ds.create_json_file(overwrite=True)